# Configuration  Monitoring TA

Add-on to monitor and parse Splunk configuration files.

Link to GitLab: https://gitlab.com/ecs_public_projects/splunk/ta-confversion  
Link to SplunkBase: https://splunkbase.splunk.com/app/4364/#/overview

## Project Status : 1.1.0

## Installation and configuration

### Splunk Components

This TA can be installed on all Splunk components including Universal Forwarders. This TA should be installed and configured on all components where configuration change tracking is desired.

This TA must be installed on Indexers and intermediate HFs, as it contains index-time transforms. 

This TA must be installed on Search Heads, as it comes bundled with important KOs for viewing the indexed configuration data. 

### Configuration

> Note: The app in the GitLab repo comes pre-configured with files in `local/`. The version on SplunkBase **is not preconfigured**, and requires the following manual steps. If the app is not configured, data will not be collected, and the app will not be usable. 

Create an index (local/indexes.conf) if you do not wish to ingest these logs into main. (recommended)

Create local/macros.conf and override the `conf_files_index` to point at the index you chose in the step above. 

Create local/inputs.conf to designate the index and enable the inputs. 

To get started quickly, you may use these sample files. When placed in ta-confversion/local, they will create a "conf_files" index and start data ingestion. Please be aware that a typical best-practice would be to configure index in a seperate (dedicated) app.

indexes.conf
```conf
[conf_files]
coldPath = $SPLUNK_DB/conf_files/colddb
enableDataIntegrityControl = 0
enableTsidxReduction = 0
homePath = $SPLUNK_DB/conf_files/db
maxTotalDataSizeMB = 51200
thawedPath = $SPLUNK_DB/conf_files/thaweddb
``` 

inputs.conf
```conf
[monitor://$SPLUNK_HOME/etc/(*.conf|(local|default).meta)]
index = conf_files
disabled = false

[monitor://$SPLUNK_HOME/etc/apps/*/*/(*.conf|(local|default).meta)]
index = conf_files
disabled = false

[monitor://$SPLUNK_HOME/etc/system/*/(*.conf|(local|default).meta)]
index = conf_files
disabled = false

[monitor://$SPLUNK_HOME/etc/deployment-apps/*/*/(*.conf|(local|default).meta)]
index = conf_files
disabled = false

[monitor://$SPLUNK_HOME/etc/master-apps/*/*/(*.conf|(local|default).meta)]
index = conf_files
disabled = false

[monitor://$SPLUNK_HOME/etc/slave-apps/*/*/(*.conf|(local|default).meta)]
index = conf_files
disabled = false

[monitor://$SPLUNK_HOME/etc/shcluster/apps/*/*/(*.conf|(local|default).meta)]
index = conf_files
disabled = false

[monitor://$SPLUNK_HOME/etc/users/*/*/*/(*.conf|(local|default).meta)]
index = conf_files
disabled = false
```

macros.conf
```conf
[conf_files_index]
definition = (index="conf_files")
```

#### File change monitor polling interval

By default, Splunk logs changes in the `$SPLUNK_HOME/etc/` directory every 10 minutes. In some cases, it may be desirable to do this more frequently, to ensure that no filechanges are missed. If this is desired, add the following to local/inputs.conf:
```conf
[fschange:$SPLUNK_HOME/etc]
#poll every 30 seconds
pollPeriod = 30
```

This change can cause added load on the system. Ensure you are comfortable with that before implementing this change. 

### Security

> Note: With the default permissions the TA will only be visilbe to / accessible by, administrators.

This TA exposes potentially sensitive information to users. This includes any passwords/tokens/usernames contained within conf files on the instance. 

It is highly recommended that the index this TA uses be made accessible solely to administrators to prevent information disclousre to unauthorised parties. 

### Configuration Files Contained Within the Add-on
- app.conf
- indexes.conf
- inputs.conf
- props.conf
- transforms.conf

## Usage & Searches

### Macros

These are some convenience macros; they allow for quickly filtering down the data using common patterns. 
Additional configuration for these macros can be found as comments in the default/macros.conf file

 - `conf_files_index` : This macro should have been updated by the administrator to point at the index where conf_file data is being ingested. (Defaults to main index (not recommended))
 - `include_deletions(4)` : This macro can be used to ensure that file deletions are included in the results returned from the search. Please consult "defaults/macros.conf" for argument details.
 - `normalize_multiline_values` : This macro can be used mid search to normaliza any multiline config attribute values. It basically removes the "\n\" pattern.
 - `confdata2mv` : Converts the attribute lines of a stanza to an mvfield. This makes them easier to work with under certain circumstances. 
 - `mostrecent` and `mostrecent_uo` : Finds the most recent stanzas from those available. The former maintains ordering of events at cost of performance. The latter does not maintain order, but is faster. 
 - `latest` : Finds the latest version of stanzas. Similar to `mostrecent` but uses streamstats instead of sorting. This can work better when there are more events, but can also have a performance penalty under certain circumstances. 


# Majority Contributors

ECS Security (Sam Hague, Tomasz Dziwok, Damien Chillet)
 - Sam Hague - Starting the project
 - Tomasz Dziwok - Improvements & features
 - Damien Chillet - Improvements & features  

## Development

Please track issues on GitLab. Merge requests are welcome, but may not be addressed immediately. 

# Support

Support will be provided by the developers on a best-effort basis. The developers make no commitment to continued development. The software is provided as is, and the developer accepts no responsibility for any issues with the software, or which may result as a consequence of using the software to the fullest extent permissible by the law.

Please find the license for this software here: https://github.com/d3/d3/blob/master/LICENSE
